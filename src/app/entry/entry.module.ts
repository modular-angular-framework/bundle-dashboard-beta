import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EntryRoutingModule } from './entry-routing.module';
import { OneComponent } from './one/one.component';
import { TwoComponent } from './two/two.component';
import { EntryComponent } from './entry.component';

@NgModule({
  imports: [CommonModule, EntryRoutingModule],
  declarations: [EntryComponent, OneComponent, TwoComponent]
})
export class EntryModule {}
