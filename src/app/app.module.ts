import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { StoreModule } from '@ngrx/store';

import { AppComponent } from './app.component';
import { EntryModule } from './entry/entry.module';
import { EntryComponent } from './entry/entry.component';

/**
 * AppModule is used for development and integration testing.
 * It is not part of the exported bundle.
 */
@NgModule({
  declarations: [AppComponent, EntryComponent],
  imports: [BrowserModule, StoreModule.forRoot({}), EntryModule],
  bootstrap: [AppComponent]
})
export class AppModule { }
